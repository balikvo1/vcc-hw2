# 1 Trasovani na tap
- budu trasovat komunikaci (ping) z instance na cmp s instanci na ctl
- na cmp:
- `docker exec -it nova_libvirt bash -i`
- `virsh list`
- `cat /etc/libvirt/qemu/instance-00000001.xml`
- ![](Pasted%20image%2020220605132749.png)
- v konzoli instance na cmp nodu pak pustim ping a necham bezet
- ![](Pasted%20image%2020220605191322.png)
- na cmp nodu pak tcpdump na interface z xml deskriptoru, na ktery je eth0 v demo instanci primo napojen
- `tcpdump -i tapa390e02d-ab`
- ![](Pasted%20image%2020220605191646.png)
# 2 East-west provoz ve Wiresharku
- znovu budu poustet ping z instance na cmp nodu na instanci na ctl nodu
- pozorovat budu ens3 interface, pres ktery jde veskera komunikace, ktera jde ven z nodu
	- tedy komunikace uz bude zarucene zabalena ve VXLAN
- `tcpdump -n -i ens3 udp -w data_dump`
- soubor s komunikaci prekopiruju na svoje PC s Wireshark
	- `scp root@10.38.6.214:data_dump .`
- ![](Pasted%20image%2020220605231403.png)
- v seznamu ramcu jsou vypsane IP adresy vnorene komunikace
- v detailu pak je videt VNID
	- ![](Pasted%20image%2020220605231559.png)
- a take IP adresy nosne UDP komunikace

# 3 diagram east-west komunikace
![](test.png)

- tap interface dane instance jsem zjistil uz v 1. casti (a take konkretni nazev qbr bridge)
- konkretni jmena qvo a portu na br-tun zjistim v kontejneru s openvswitchem
	- ![](Pasted%20image%2020220606013716.png)
- jmeno qvb portu muzu vycist z `ip l`, kde je sparovany s nalezenym qvo
	- ![](Pasted%20image%2020220606013823.png)
- interface 3 pak musi byt ens3, ktery vede ven z nodu
- tento diagram popisuje pouze node cmp
	- pro node ctl
		- diagram by se zkomplikoval o dalsi prvky (jelikoz je i network nodem), jako je treba router, a samozrejme nazvy by byly jine
		- tato east-west komunikace nicmene probiha v ramci jedne interni (VX)LAN site, tudiz router neni potreba (pri north-south komunikaci uz by potreba byl)
		- provoz by tedy prosel pres stejne prvky a jejich nazvy bych nasel stejnym zpusobem